Notice for utilisation 

* * Why this project ?
This program was made in 2020 for a school project in ENSE3 engineering school about MODIS Snow Teledection in the area of Grenoble, and larger the Alps area.
The program allows the user to specify a date in 2019 and choose a product. Then the program will download the needed HDF files, then compute the associated view, saved as .png.

* * How do you use it ?
* Download the .zip of this git.
* Unzip all the files in the directory where you installed Anaconda.
* Open Anaconda, get in a new environnement and where you'll have to have the following packages : 
- > os, shutil, sys, getpass, glob, base64, itertools, json, ssl, urllib3, numpy, matplotlib, geopandas, rasterio, earthpy, osgeo, PIL 
[if needed, you can download the missing packages on https://anaconda.org/]
Be careful, you need the latest version of Anaconda and Spyder (the code has been written for Spyder 4.1.2)
* Then, run the python file 'Main_Program_to_Execute.py' in the folder 'script' of the folder 't-l-detection-modis-master' you get from the dezipping.
* Answer the instructions and you'll get the downloaded and viewed chosen MODIS product of the Alps area in this same folder.

* The other scripts are used in this main script and what they actually do is detailled in their code.

FOR MORE INFORMATION, LOOK AT THE REPORT_MODIS.PDF
